$(function () {
    $("#box").jqCarouselImg({
        imgLen: 3,
        imgUrl: ['assets/img/news.jpeg', 'assets/img/update4.jpeg', 'assets/img/download.png'],
        speed: 4000,
        direction: false,
    })
});

var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-36251023-1']);
_gaq.push(['_setDomainName', 'jqueryscript.net']);
_gaq.push(['_trackPageview']);

(function () {
    var ga = document.createElement('script');
    ga.type = 'text/javascript';
    ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') +
        '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ga, s);
})();

;
(function ($) {

    let defaults = {
        imgUrl: [],
        imgLen: 4,
        speed: 4000,
        direction: false,
    }

    function carouselImg(element, options) {
        this.element = element;
        this.options = $.extend({}, defaults, options);

        this.init(this.options);
    }

    carouselImg.prototype = {
        init: function (options) {
            let self = this;

            let _imgUrl = options.imgUrl;
            let _imgLen = options.imgLen;
            let _carouselDirection = options.direction;
            const _speed = options.speed;
            let _elem = this.element;

            let $imgUl = $('<ul class="imgUl"></ul>');
            _elem.append($imgUl);

            for (let i = 0; i < _imgLen; i++) {
                var $imgLi = $('<li class="imgLi"></li>');
                $(".imgUl").append($imgLi);
                var $img = $('<img class="carouselImg" />');
                for (let j = 0; j < _imgLen; j++) {
                    $imgLi.append($img);
                }
            }
            for (let k = 0; k < $(".carouselImg").length; k++) {
                $(".carouselImg")[k].src = _imgUrl[k];
            }

            let $btnUl = $('<ul class="btnUl"></ul>');
            _elem.append($btnUl);
            for (let i = 0; i < _imgLen; i++) {
                var $btnLi = $('<li class="btnLi"></li>');
                $btnUl.append($btnLi);
            }

            let $prev = $('<span class="btn-img prevImg">&lt;</span>');
            let $next = $('<span class="btn-img nextImg">&gt;</span>');
            _elem.append($prev);
            _elem.append($next);

            _elem.mouseover(function () {
                $(".btn-img").show();
            });
            _elem.mouseout(function () {
                $(".btn-img").hide();
            })

            let _count = 0;
            let _timer;
            let _islast = false;
            let _liWidth = $imgLi[0].offsetWidth;

            function moveImg() {
                _timer = setInterval(function () {
                    if (_islast == false) {
                        _count++;
                        $imgUl[0].style.transform = "translate(" + -_liWidth * _count + "px)";
                        if (_count >= _imgLen - 1) {
                            _count = _imgLen - 1;
                            _islast = true;
                        }
                    } else {
                        if (_carouselDirection) {
                            _count = 0;
                            $imgUl[0].style.transform = "translate(" + 0 + "px)";
                            _islast = false;
                        } else {
                            _count--;
                            $imgUl[0].style.transform = "translate(" + -_liWidth * _count + "px)";
                            if (_count <= 0) {
                                _count = 0;
                                _islast = false;
                            }
                        }

                    }
                    for (let i = 0; i < $(".btnLi").length; i++) {
                        $(".btnLi")[i].style.backgroundColor = "rgba(255,255,255,0.2)";
                    }
                    $(".btnLi")[_count].style.backgroundColor = "rgba(255,255,255,0.8)";
                }, _speed);
            }
            moveImg();

            $(".btnUl").delegate('li', 'mouseover', function (ev) {
                clearInterval(_timer);
                if ($(this).index() == _imgLen - 1) {
                    _islast = true;
                }
                if ($(this).index() == 0) {
                    _islast = false;
                }
                _count = $(this).index();
                $imgUl[0].style.transform = "translate(" + -_liWidth * _count + "px)";
                $(this).css("backgroundColor", "rgba(255,255,255,0.8)");
                $(this).siblings().css("backgroundColor", "rgba(255,255,255,0.2)");
            });

            $(".btnUl").delegate('li', 'mouseout', function (ev) {
                moveImg();
            });

            $(".btn-img").mouseover(function () {
                clearInterval(_timer);
            });
            $(".btn-img").mouseout(function () {
                moveImg();
            })

            $(".btn-img").each(function () {
                $(this).click(function () {
                    if ($(this).index() == _imgLen - 1) {
                        _islast = true;
                    }
                    if ($(this).index() == 0) {
                        _islast = false;
                    }
                    if ($(this).hasClass("prevImg")) {
                        if (_count != 0) {
                            _count--;
                        }
                    }
                    if ($(this).hasClass("nextImg")) {
                        if (_count != _imgLen - 1) {
                            _count++;
                        }
                    }
                    $imgUl[0].style.transform = "translate(" + -_liWidth * _count + "px)";
                    for (let i = 0; i < $(".btnLi").length; i++) {
                        $(".btnLi")[i].style.backgroundColor = "rgba(255,255,255,0.2)";
                    }
                    $(".btnLi")[_count].style.backgroundColor = "rgba(255,255,255,0.8)";
                });
            });
        }
    }

    $.fn.jqCarouselImg = function (options) {
        new carouselImg(this, options);
    }

})(jQuery);