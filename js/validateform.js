function validateForm() {
  let username = document.forms["form"]["username"].value;
  let email = document.forms["form"]["email"].value;
  let password = document.forms["form"]["password"].value;
  let password_confirm = document.forms["form"]["password_confirm"].value;
  let nationality = document.forms["form"]["nationality"].value;
  let dob = document.forms["form"]["dob"].value;
  let term = document.forms["form"]["term"].value;

  if (username == "") {
    alert("Username must be filled out");
    return false;
  }

  if (email == "") {
    alert("Email must be filled out");
    return false;
  }

  if (password == "") {
    alert("Password must be filled out");
    return false;
  }

  if (password_confirm == "") {
    alert("Confirm Password must be filled out");
    return false;
  } else if (password_confirm !== password) {
        alert("Passwords doesn't match");
        return false;
  }

  if (nationality == "") {
    alert("Nationality must be filled out");
    return false;
  }

  if (dob == "") {
    alert("Date of Birth must be filled out");
    return false;
  }

  if (term !== "checked") {
    alert("You must agree with term and conditions");
    return false;
  }
}